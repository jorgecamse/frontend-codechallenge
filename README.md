# ToDo App

> This is an app build with Vue.js.
[Live DEMO](https://goi-todoapp.herokuapp.com/)

## Build setup

### Install packages

``` bash
# Install dependencies
yarn install
```

### Once installed...

``` bash
# Serve with hot reload at localhost:8080
yarn serve

# Build for production with minification
yarn build

# Run unit tests
yarn test

# Run e2e tests
yarn e2e

# Run linter and fix errors
yarn lint
```
