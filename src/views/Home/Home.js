import { mapState, mapMutations } from 'vuex';

import services from '@/services/tasks';

import TodoSearch from '@/components/TodoSearch/TodoSearch.vue';
import TodoInput from '@/components/TodoInput/TodoInput.vue';
import TodoItem from '@/components/TodoItem/TodoItem.vue';

export default {
  name: 'home',
  components: {
    TodoSearch,
    TodoInput,
    TodoItem,
  },
  data() {
    return {
      isLoading: false,
      titleSearch: '',
    };
  },
  beforeMount() {
    const initTask = 0;
    const maxTasks = 10;

    /*
      Antes de insertar el componente en el DOM, en tiempo de la primera
      renderización del componente realizamos la peticion http para obtener la
      lista de tareas iniciales usando axios.
      Estamos usando Vuex para controlar el estado de la aplicación.
      Una vez que tenemos la respuesta haciendo uso de las mutaciones
     (funciones que se encargan de cambiar el valor de nuestro estado)
     seteamos nuestro lista inicial de tareas.
    */
    services.getTasks(initTask, maxTasks)
      .then((tasks) => {
        this.setTasks(tasks.data);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        this.isLoading = false;
      });
  },
  /* Computadas son funciones que son consumidas como propiedades y que son
   actualizadas cuando una propiedad de las que depende es modificada. */
  computed: {
    ...mapState([
      'tasks',
    ]),
    // Computada para obtener la lista de tareas pendientes
    pendingTasks() {
      return this.tasks.filter(t => !t.completed);
    },
    // Computada para obtener las tareas que coinciden con el término buscado
    filteredTasks() {
      return this.tasks.filter(t => t.title.includes(this.titleSearch));
    },
  },
  methods: {
    add(text) {
      this.addTask({ title: text, completed: false });
    },
    search(text) {
      this.titleSearch = text;
    },
    ...mapMutations([
      // mapea this.setTasks() to this.$store.commit('setTasks')
      'setTasks',
      // mapea this.addTask() to this.$store.commit('addTask')
      'addTask',
      // mapea this.toggleTaskStatus() to this.$store.commit('toggleTaskStatus')
      'toggleTaskStatus',
      // mapea this.deleteTask() to this.$store.commit('deleteTask')
      'deleteTask',
    ]),
  },
};
