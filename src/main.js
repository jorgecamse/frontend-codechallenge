import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import feather from 'vue-icon';

import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './i18n';

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);

Vue.use(feather, {
  name: 'v-icon',
  props: {
    baseClass: {
      type: String,
      default: 'v-icon',
    },
    classPrefix: {
      type: String,
      default: 'v-icon-',
    },
  },
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
