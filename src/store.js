import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    tasks: [],
  },
  // Las mutaciones son funciones que se encargan de cambiar el valor de nuestro estado
  mutations: {
    // Mutación para setear listado de tareas
    setTasks(state, tasks) {
      // eslint-disable-next-line
      state.tasks = tasks;
    },
    // Mutación para cambiar el status de una tarea (pendiente o completada)
    toggleTaskStatus(state, { task, status }) {
      const targetTask = state.tasks.find(e => e === task);
      targetTask[status] = !targetTask[status];
    },
    // Mutación para añadir una nueva tarea a la lista
    addTask(state, task) {
      state.tasks.push(task);
    },
    // Mutación para borrar una nueva tarea de la lista
    deleteTask(state, { task }) {
      const index = state.tasks.indexOf(task);
      state.tasks.splice(index, 1);
    },
  },
});
