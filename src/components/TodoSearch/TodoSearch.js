export default {
  name: 'todo-search',
  props: {
    placeholder: {
      type: String,
      value: 'Search task...',
    },
  },
  data() {
    return {
      text: '',
    };
  },
  /*
    Watch nos permite definir un listado de propiedades que queremos
    reaccionar a sus cambios de forma asíncrona.
    En nuestro caso queremos que cada vez que se introduzca un caracter en el
    input se dispare un evento con todo el valor actual.
  */
  watch: {
    text(val) {
      this.$emit('todo:search', val);
    },
  },
};
