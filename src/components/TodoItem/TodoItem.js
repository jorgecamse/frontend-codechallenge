import TodoCheckbox from '../TodoCheckbox/TodoCheckbox.vue';

export default {
  name: 'todo-item',
  props: {
    title: String,
    completed: Boolean,
  },
  components: {
    TodoCheckbox,
  },
};
