export default {
  name: 'todo-checkbox',
  props: {
    value: Boolean,
    name: String,
  },
};
