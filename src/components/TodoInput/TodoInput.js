export default {
  name: 'todo-input',
  props: {
    placeholder: {
      type: String,
      value: 'What needs to be done?',
    },
  },
  data() {
    return {
      text: '',
    };
  },
  methods: {
    /*
      Si el input contiene alguna cadena de texto disparamos un evento con el
      contenido cuando el usuario presiona el botón de añadir o escribe en la
      caja texto y pulsa la enter.
    */
    addItem() {
      if (this.text.length !== 0) {
        this.$emit('todo:add', this.text);
        this.text = '';
      }
    },
  },
};
