import axios from 'axios';

// Static defaults
axios.defaults.baseURL = process.env.VUE_APP_API_HOST;
axios.defaults.headers.common['Content-Type'] = 'application/json';

export default {
  /*  Servicio para obtener la lista de tareas. Como parámetros puede recibir el
   elemento desde el que empezar y un límite de elementos. */
  getTasks(start, limit) {
    return axios.get('/todos', {
      params: {
        _start: start,
        _limit: limit,
      },
    });
  },
};
