import { shallow } from '@vue/test-utils';
import HelloVue from '@/components/HelloVue.vue';

describe('HelloWorld.vue', () => {
  it('is a Vue instance', () => {
    const wrapper = shallow(HelloVue);
    expect(wrapper.isVueInstance()).toBe(true);
  });

  it('renders props.msg when passed', () => {
    const msg = 'new message';
    const wrapper = shallow(HelloVue, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
