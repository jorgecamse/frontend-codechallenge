import { shallow } from '@vue/test-utils';
import About from '@/views/About.vue';
import i18n from '@/i18n';

describe('HelloWorld.vue', () => {
  it('is a Vue instance', () => {
    const wrapper = shallow(About, { i18n });
    expect(wrapper.isVueInstance()).toBe(true);
  });
});
